-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 05, 2021 at 10:24 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `devcamp_order`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `address`, `email`, `full_name`, `phone`) VALUES
(1, '55 Trần Đình Xu Quận 1 HCM', 'tam_thien@gmail.com', 'Thiên Tâm', '0912345678'),
(2, '23 Nguyễn Đình Chiểu Q1 HCM', 'ha_nguyen@gmail.com', 'Nguyễn Hà', '0122345678'),
(3, '34 Lương Nhữ Học Q5 HCM', 'toan_tran@gmail.com', 'Trần Toàn', '09093456789'),
(4, '111 Lý Thái Tổ Q10 HCM', 'phong_ngo@gmail.com', 'Ngô Phong', '0903987654'),
(5, '545 Nguyễn Huệ Đà Nẵng', 'anh_hoang@gmail.com', 'Hoàng Anh', '0907456789');

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `order_code` varchar(255) DEFAULT NULL,
  `paid` bigint(20) DEFAULT NULL,
  `pizza_size` varchar(255) DEFAULT NULL,
  `pizza_type` varchar(255) DEFAULT NULL,
  `price` bigint(20) DEFAULT NULL,
  `voucher_code` varchar(255) DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_code`, `paid`, `pizza_size`, `pizza_type`, `price`, `voucher_code`, `customer_id`) VALUES
(1, 'OPiz101', 180000, 'S', 'Hải sản', 200000, '12354', 1),
(2, 'OPiz102', 225000, 'M', 'Hawaii', 250000, '12365', 2),
(3, 'OPiz103', 225000, 'M', 'Thịt hun khói', 250000, '12378', 3),
(4, 'OPiz104', 270000, 'L', 'Hawaii', 300000, '12389', 4),
(5, 'OPiz105', 180000, 'S', 'Hải sản', 200000, '13256', 5),
(6, 'OPiz106', 225000, 'M', 'Thịt hun khói', 250000, '13654', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL,
  `color` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `price` bigint(20) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `order_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `color`, `name`, `price`, `type`, `order_id`) VALUES
(1, 'red', 'pizza hai san', 200000, 'M', 1),
(2, 'yellow', 'pizza hawaii', 200000, 'M', 1),
(3, 'blue', 'pizza bacon', 250000, 'L', 1),
(4, 'red', 'pizza hai san', 200000, 'M', 2),
(5, 'purple', 'pizza hawaii', 150000, 'S', 3),
(6, 'green', 'pizza bacon', 250000, 'L', 4),
(7, 'gold', 'pizza hai san', 200000, 'M', 5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_dhk2umg8ijjkg4njg6891trit` (`order_code`),
  ADD UNIQUE KEY `UK_kdqb24llmcg5bi73wxp74bt5m` (`voucher_code`),
  ADD KEY `FKpxtb8awmi0dk6smoh2vp1litg` (`customer_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK18j7hot76crqfb6x6xn7mlxt6` (`order_id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `FKpxtb8awmi0dk6smoh2vp1litg` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`);

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `FK18j7hot76crqfb6x6xn7mlxt6` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
